#!/bin/bash
wget http://ftp.riken.jp/net/apache/tomcat/tomcat-8/v8.5.60/bin/apache-tomcat-8.5.60.tar.gz
tar xvzf apache-tomcat-8.5.60.tar.gz
sudo mv apache-tomcat-8.5.60 /usr/tomcat8
sudo useradd -M -d /usr/tomcat8 tomcat8
sudo chown -R tomcat8. /usr/tomcat8
sudo cat<<EOF>/etc/rc.d/init.d/tomcat8
#!/bin/bash

# Tomcat8: Start/Stop Tomcat 8
#
# chkconfig: - 90 10
# description: Tomcat is a Java application Server.

. /etc/init.d/functions
. /etc/sysconfig/network

CATALINA_HOME=/usr/tomcat8
TOMCAT_USER=tomcat8

LOCKFILE=/var/lock/subsys/tomcat8

RETVAL=0
start(){
    echo "Starting Tomcat8: "
    su - $TOMCAT_USER -c "$CATALINA_HOME/bin/startup.sh"
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && touch $LOCKFILE
    return $RETVAL
}

stop(){
    echo "Shutting down Tomcat8: "
    $CATALINA_HOME/bin/shutdown.sh
    RETVAL=$?
    echo
    [ $RETVAL -eq 0 ] && rm -f $LOCKFILE
    return $RETVAL
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    *)
        echo $"Usage: tomcat8 {start|stop|restart}"
        exit 1
        ;;
esac
exit $?
EOF

sudo sed -i 's/""/"$1"/' /etc/rc.d/init.d/tomcat8