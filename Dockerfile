FROM amazonlinux:1

LABEL maintainer "chee <osamu.takahashi@miraitranslate.com>"

ARG EC2_PARADOX_API_URI
ARG EC2_BOXWOOD_API_URI
ARG PHP_REDIS_BUILD_DIR="/tmp/phpredis"

ARG SAMLSP_DB_NAME
ARG SAMLSP_DB_USER
ARG SAMLSP_DB_PASSWORD
ARG SAMLSP_DB_HOST
ARG REDIS_HOST
ARG REDIS_PORT

RUN yum -y reinstall glibc-common
ENV LANG ja_JP.UTF-8

RUN yum -y update && \
  yum -y install sudo curl zip less vim procps diff && \
  yum -y install openssh-server openssh && \
  yum -y install wget && \
  yum -y install httpd24 mod24_ssl && \
  yum -y install php56 php56-mbstring php56-devel php56-pecl-igbinary php56-pdo php56-pgsql && \
  yum -y install git && \
  yum -y install gcc && \
  yum -y install epel-release && \
  yum-config-manager --enable epel && \
  yum -y install ssmtp && \
  rm -rf /var/cache/yum/* && \
  yum clean all

RUN mkdir ${PHP_REDIS_BUILD_DIR}

WORKDIR ${PHP_REDIS_BUILD_DIR}
RUN git clone -b 4.2.0 https://github.com/phpredis/phpredis.git

WORKDIR ${PHP_REDIS_BUILD_DIR}/phpredis
RUN phpize && \
  ./configure && \
  make && \
  make install && \
  echo 'extension=redis.so'| tee -a /etc/php.d/redis.ini && \
  rm -rf ${PHP_REDIS_BUILD_DIR}/phpredis

COPY docker/conf/php.ini /etc/
COPY docker/conf/php.conf /etc/httpd/conf.d/
COPY docker/conf/httpd.conf /etc/httpd/conf/
COPY docker/conf/ssl.conf /etc/httpd/conf.d/
COPY docker/conf/autoindex.conf /etc/httpd/conf.d/
COPY docker/conf/php.ini /etc/
COPY docker/conf/ssmtp.conf /etc/ssmtp/

# 開発環境では /var/www はローカルのディレクトリをマウントする
#ADD domain /var/www/domain/
#ADD html /var/www/html/
#ADD views /var/www/views/
#ADD autoload.php /var/www/
COPY composer.json /var/www/
COPY composer.lock /var/www/
COPY composer.phar /var/www/
#ADD conf.php /var/www/
#ADD domain.php /var/www/
#ADD Version /var/www/
#RUN rm -rf /var/www/views/cache/*

#RUN chmod 777 /var/www/views/cache/

WORKDIR /var
RUN mkdir simplesamlphp
RUN curl -OL https://github.com/simplesamlphp/simplesamlphp/releases/download/v1.16.3/simplesamlphp-1.16.3.tar.gz
RUN tar xzvf simplesamlphp-1.16.3.tar.gz -C /var/simplesamlphp --strip-components 1

WORKDIR /var/simplesamlphp/
COPY samlsp/composer.* ./

WORKDIR /var/simplesamlphp/config
COPY samlsp/config/* ./

WORKDIR /var/simplesamlphp/cert
COPY samlsp/cert/* ./

WORKDIR /var/simplesamlphp/www
COPY samlsp/www/* ./

RUN chmod 777 /var/simplesamlphp/log
RUN chmod 777 /var/log/httpd/

WORKDIR /var/www
USER root
RUN php composer.phar install

WORKDIR /var/simplesamlphp
USER root
RUN php /var/www/composer.phar install

# RUN sudo mv /tmp/ParadoxWeb/* /var/www/

# docker-composeで立てる用のendpointへ修正
RUN sudo sed -i -e 's/\${REDIS_ENDPOINT}/redis/g' /etc/httpd/conf.d/php.conf

RUN : "PHPの環境変数設定" && { \
  echo "SetEnv EC2_PARADOX_API_URI ${EC2_PARADOX_API_URI}"; \
  echo "SetEnv EC2_BOXWOOD_API_URI ${EC2_BOXWOOD_API_URI}"; \
  echo "SetEnv SAMLSP_DB_NAME ${SAMLSP_DB_NAME}"; \
  echo "SetEnv SAMLSP_DB_USER ${SAMLSP_DB_USER}"; \
  echo "SetEnv SAMLSP_DB_PASSWORD ${SAMLSP_DB_PASSWORD}"; \
  echo "SetEnv SAMLSP_DB_HOST ${SAMLSP_DB_HOST}"; \
  echo "SetEnv SAMLSP_REDIS_HOST ${REDIS_HOST}"; \
  echo "SetEnv SAMLSP_REDIS_PORT ${REDIS_PORT}"; \
  } | tee -a /etc/httpd/conf.d/env.conf

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
